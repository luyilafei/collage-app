import Vue from 'vue'
import App from './App'
import uView from "uview-ui";
Vue.use(uView);
Vue.config.productionTip = false

import { http,api } from '@/config/common.js' // 全局挂载引入，配置相关在该index.js文件里修改
Vue.prototype.$http = http
Vue.prototype.$api = api

App.mpType = 'app'
Vue.prototype.back = function (){
  uni.navigateBack({
      delta: 1
  });
}
Vue.prototype.backhome = function (){
  uni.switchTab({
      url: '/pages/index/index'
  });
}
Vue.prototype.link = function (url){
  uni.navigateTo({
      url: url
  });
}
const app = new Vue({
    ...App
})
app.$mount()

import Request from 'luch-request' // 使用npm
const http = new Request()
export {http}

http.setConfig((config) => { /* config 为默认全局配置*/
    config.baseURL = 'http://127.0.0.5/'; /* 根域名 */
    config.header = {
		//'content-type':'application/x-www-form-urlencoded'
		'content-type':'application/json'
    }
    return config
})
//请求前拦截
http.interceptors.request.use((config) => { // 可使用async await 做异步操作
  config.header = {
    ...config.header,
  }
  //获取存储的token
  const token = uni.getStorageSync('token');
  config.header.token = token;
  return config
}, config => { // 可使用async await 做异步操作
  return Promise.reject(config)
})


// 请求后拦截器
http.interceptors.response.use((response) => {
	if(response.data.code==-1){
		  uni.clearStorageSync();
		  uni.navigateTo({
		  	url:"/pages/login/login"
		  })
	}
    return response.data
}, (response) => {
  //未登录时清空缓存跳转
  if(response.statusCode ==401){
	  uni.clearStorageSync();
	  uni.navigateTo({
	  	url:"/pages/login/login"
	  })
  }
  if(response.data.code==-1){
	  uni.clearStorageSync();
	  uni.navigateTo({
	  	url:"/pages/login/login"
	  })
  }
  return Promise.reject(response)
})

/**
 * 
 */
/*
#允许跨域请求的域， *代表所有
add_header  'Access-Control-Allow-Origin'  *; 

#允许带上cookie请求
add_header  'Access-Control-Allow-Credentials'  'true'; 

#允许请求的方法， 比如 GET/POST/PUT/DELETE
add_header  'Access-Control-Allow-Methods'  *; 

#允许请求的header
add_header  'Access-Control-Allow-Headers'  *;
*/